from datetime import datetime
from koodit import db, ma
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields

class Gamesession(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    userid = db.Column(db.Integer)
    time = db.Column(db.DateTime, default=datetime.utcnow)
    answers = db.relationship('Answer',backref='sessionref',lazy='dynamic')

class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quizid = db.Column(db.Integer)
    session = db.Column(db.Integer(), db.ForeignKey('gamesession.id'))
    score = db.Column(db.Integer)

class GamesessionSchema(ma.Schema):
    class Meta:
        fields = ('id','userid','time')

class AnswerSchema(ma.Schema):
    class Meta:
        fields = ('id','quizid','session','score')

