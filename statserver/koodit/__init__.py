from flask import Flask
from config import konffit
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow

serveri = Flask(__name__)
serveri.config.from_object(konffit)
db = SQLAlchemy(serveri)
ma = Marshmallow(serveri)
migrate = Migrate(serveri,db)

from koodit import reitit, mallit 
