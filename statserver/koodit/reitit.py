import random
from flask import (
    jsonify,
    make_response,
    render_template,
    request,
    send_from_directory
)

from koodit import serveri, db
from koodit.mallit import Gamesession, GamesessionSchema
from koodit.mallit import Answer, AnswerSchema

@serveri.route('/gamesessions', methods = ['GET'])
def list_sessions():
    get_gamesessions = Gamesession.query.all()
    gamesession_schema = GamesessionSchema(many=True)
    gamesessions = gamesession_schema.dump(get_gamesessions)
    return make_response(jsonify(gamesessions))

@serveri.route('/gamesessions/user/<id>', methods = ['GET'])
def list_user_sessions(id):
    get_gamesessions = Gamesession.query.filter_by(userid=id).all()
    gamesession_schema = GamesessionSchema(many=True)
    gamesessions = gamesession_schema.dump(get_gamesessions)
    return make_response(jsonify(gamesessions))

@serveri.route('/gamesessions', methods = ['POST'])
def create_gamesession():
    uid = int(request.json['userid'])
    gs = Gamesession(userid=uid)
    db.session.add(gs)
    db.session.commit()
    gs_schema = GamesessionSchema()
    return gs_schema.jsonify(gs)

@serveri.route('/answers', methods = ['GET'])
def get_answers():
    get_answers = Answer.query.all()
    answer_schema = AnswerSchema(many=True)
    answers = answer_schema.dump(get_answers)
    return make_response(jsonify(answers))

@serveri.route('/answers', methods = ['POST'])
def create_answer():
    quiz_id = int(request.json['quizid'])
    session_id = int(request.json['session'])
    score_value = int(request.json['score'])
    ans_obj = Answer(quizid=quiz_id,session=session_id,score=score_value)
    db.session.add(ans_obj)
    db.session.commit()
    ans_schema = AnswerSchema()
    return ans_schema.jsonify(ans_obj)

@serveri.route('/answers/session/<id>', methods = ['GET'])
def get_session_answers(id):
    get_answers = Answer.query.filter_by(session=id).all()
    answer_schema = AnswerSchema(many=True)
    answers = answer_schema.dump(get_answers)
    return make_response(jsonify(answers))

@serveri.route('/answers/quiz/<id>', methods = ['GET'])
def get_quiz_answers(id):
    get_answers = Answer.query.filter_by(quizid=id).all()
    answer_schema = AnswerSchema(many=True)
    answers = answer_schema.dump(get_answers)
    return make_response(jsonify(answers))
