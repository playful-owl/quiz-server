import os
import psycopg2

basedir = os.path.abspath(os.path.dirname(__file__))

class konffit(object):
    SECRET_KEY = 'avainsalaisuusavain'
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
    # SQLALCHEMY_DATABASE_URI = 'postgresql+psycopg2://postgres:docker@localhost:5432'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
