#! /bin/bash

# this script is for local testing outside docker

rm -rf migrations/
rm -rf __pycache__/
rm -rf koodit/__pycache__/
