import pandas
from koodit import db
from koodit.mallit import Quiz

def fun():
    d = pandas.read_csv('data/sanat4.csv',sep=';')
    for i in range(d.shape[0]):
        cword = d.iloc[i,0]
        objs = Quiz.query.filter_by(word=cword).all()
        if len(objs) == 0:
            clanguage = d.iloc[i,1]
            cgender = d.iloc[i,2]
            ckind = d.iloc[i,3]
            cchapter = int(d.iloc[i,4])
            cfinnish = d.iloc[i,5]
            koko: str = cword+' '+clanguage+' '+cgender+' '+ckind+' '+str(cchapter)+' '+cfinnish
            print(koko)
            q = Quiz(word=cword,language=clanguage,gender=cgender,
                    kind=ckind,chapter=cchapter,finnish=cfinnish)
            db.session.add(q)
            db.session.commit()

fun()

