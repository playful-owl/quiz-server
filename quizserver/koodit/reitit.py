import json
import random
import requests
from flask import (
    flash,
    jsonify,
    make_response,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for
)

from werkzeug.urls import url_parse
from flask_login import current_user, login_user, login_required, logout_user
from koodit import serveri, db
from koodit.mallit import Quiz, QuizSchema, User
from koodit.lomakkeet import LoginForm

## file access routes
## todo: serve via nginx/statics

@serveri.route('/js/<path:path>')
def js(path):
    return send_from_directory('js',path)

@serveri.route('/css/<path:path>')
def css(path):
    return send_from_directory('css',path)

@serveri.route('/materiaalit/<path:path>')
def materiaalit(path):
    return send_from_directory('materiaalit',path)

## quiz question and answer routes

@serveri.route('/question',methods=['GET'])
def question():
    kysymykset = Quiz.query.all()
    inds = random.sample(range(len(kysymykset)),4)
    oikea = kysymykset[inds[0]]
    retrieved_q = str(oikea.finnish)
    retrieved_a = [str(kysymykset[i].word) for i in inds]
    shuffled_a = random.sample(retrieved_a, len(retrieved_a))
    correct_index = shuffled_a.index(retrieved_a[0])
    session['qid'] = int(oikea.id)
    session['correct'] = correct_index
    q = {'q': retrieved_q,
         'a': shuffled_a}
    return jsonify(q)

@serveri.route('/answer',methods=['GET'])
def answer():
    vastaus = request.args['ans']
    headers = {'Content-Type': 'application/json'}
    if int(vastaus) == session['correct']:
        payload = {'quizid': session['qid'],
                   'session': session['gamesession_id'],
                   'score': 1}
        res = {'score': 1}
    else:
        payload = {'quizid': session['qid'],
                   'session': session['gamesession_id'],
                   'score': 0}
        res = {'score': 0}
    r = requests.post('http://statcontainer:5001/answers',
                      headers = headers,
                      data = json.dumps(payload))
    ## statrecv = r.json() # response is not needed for anything
    return jsonify(res)

## page routes

@serveri.route('/peli')
@login_required
def peli():
    uid = current_user.id
    headers = {'Content-Type': 'application/json'}
    payload = {'userid': uid}
    r = requests.post('http://statcontainer:5001/gamesessions',
                      headers = headers,
                      data = json.dumps(payload))
    statrecv = r.json()
    sessionid = statrecv['id']
    session['gamesession_id'] = sessionid
    return render_template('peli.html')

@serveri.route('/')
@serveri.route('/index')
@login_required
def indeksi():
    uid = current_user.id
    uname = current_user.username
    ## get gamesession count
    targeturl = 'http://statcontainer:5001/gamesessions/user/' + str(uid)
    ur = requests.get(targeturl)
    ustatrecv = ur.json()
    ugs_len = len(ustatrecv)
    ## todo: get answer count?
    return render_template('indeksi.html',
                           username = uname,
                           sessioncount = ugs_len)

## data/api routes, called via ajax

@serveri.route('/api/questions',methods=['GET'])
def question_data():
    targeturl = 'http://statcontainer:5001/answers'
    ur = requests.get(targeturl)
    ustatrecv = ur.json()
    qids = [ustatrecv[i]['quizid'] for i in range(len(ustatrecv))]
    scores = [ustatrecv[i]['score'] for i in range(len(ustatrecv))]
    uqids = list(set(qids)) ## removes duplicates
    uscores = [0] * len(uqids)
    uoccs = [0] * len(uqids)
    for i in range(len(scores)):
        insert_idx = uqids.index(qids[i])
        uscores[insert_idx] += scores[i]
        uoccs[insert_idx] += 1
    words = [None] * len(uqids)
    for i in range(len(uqids)):
        quiz = Quiz.query.filter_by(id=uqids[i]).first()
        words[i] = quiz.word
        uoccs[i] = uoccs[i] - uscores[i]
    res = {'words': words,
           'scores': uscores,
           'attempts': uoccs}
    return res

@serveri.route('/api/playersessions',methods=['GET'])
def playersession_data():
    uid = current_user.id
    targeturl = 'http://statcontainer:5001/gamesessions/user/' + str(uid)
    ur = requests.get(targeturl)
    ustatrecv = ur.json()
    ugs_len = len(ustatrecv)
    ## get all answers
    ## get corresponding quiz words
    gs_attempts = [0]*ugs_len
    gs_scores = [0]*ugs_len
    gs_times = [ustatrecv[i]['time'] for i in range(ugs_len)]
    ugs_ids = [ustatrecv[i]['id'] for i in range(ugs_len)]
    ## get all answers for all sessions
    for i in range(ugs_len):
        target_gs_url = 'http://statcontainer:5001/answers/session/' + str(ugs_ids[i])
        gsr = requests.get(target_gs_url)
        gsrecv = gsr.json()
        scores = [gsrecv[i]['score'] for i in range(len(gsrecv))]
        gs_scores[i] = sum(scores)
        gs_attempts[i] = len(gsrecv) - gs_scores[i]
    res = {'times': gs_times,
           'scores': gs_scores,
           'attempts': gs_attempts }
    return res

## login/logout routes

@serveri.route('/login',methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('indeksi'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('invalid credentials')
            return redirect(url_for('login'))
        login_user(user, remember=False)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('indeksi')
        return redirect(next_page)
    return render_template('login.html', title='kirjaudu', form=form)

@serveri.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

## leftovers

@serveri.route('/quiz/<id>', methods = ['GET'])
def get_quiz_by_id(id):
    get_quiz = Quiz.query.get(id)
    quiz_schema = QuizSchema()
    q = quiz_schema.dump(get_quiz)
    return make_response(jsonify({"quiz": q}))
