'use strict';

const freeze = obj => Object.freeze(obj);

function enumeration(...members){
  let member_values = {};
  for (let member of members){
    if(typeof member === "string"){
      member_values[member] = freeze({ value: member });
    }else if(typeof member === "object"){
      const key = Object.keys(member)[0];
      member_values[key] = freeze({ value: member[key] });
    }
  }
  return freeze(member_values);
}

const game_state = enumeration(
  "intro",
  "search",
  "carry",
  "loading",
  "scores"
);

let options = {
  gravity: 0.25,
};

// size 1200 x 600

var timer;
var cursors;
const target_xs = [80,383,716,1050];
const debris_xlocs = [150,300,450,500,550,600,700,800,900,1000,1100];
let cargotext = "kysymys";
let answertexts = ["vastaus0","vastaus1","vastaus2","vastaus3"];
let pisteet = 0;

const WALL = 4;
const PLAYER = 5;
const HOOK = 6;
const CARGO = 7;

class Lentorahti extends Phaser.Scene
{

  constructor(){
    super();
  }

  preload(){
    this.load.image('alus', 'materiaalit/x2kship.png');
  }

  create(){

    this.matter.world.update30Hz();
    
    this.state = game_state["search"];
    
    this.matter.world.setBounds();

    // static terrain
    this.terrain = [];
    this.terrain.push(this.matter.add.rectangle(550, 500, 200, 32, { isStatic: true }));
    this.terrain[0].label = WALL;
    this.terrain.push(this.matter.add.rectangle(0, 590, 2400, 10, { isStatic: true }));
    this.terrain[1].label = WALL;

    // dynamic terrain
    this.debris = [];
    for(let i = 0; i < debris_xlocs.length; i++){
      this.debris.push(this.matter.add.circle(debris_xlocs[i], 450, 16));
    }


    // cargo targets
    this.target_texts = [];
    for(let i = 0; i < 4; i++){
      this.target_texts.push(this.add.text(target_xs[i],20,answertexts[i],
					   { font: '32px Arial', fill: '#ffff00' }));
      
    }

    this.targets = [];
    for(let i = 0; i < 4; i++){
      this.targets.push(this.matter.add.gameObject(this.target_texts[i],
						   { isSensor: true,
						     isStatic: true,
						     label: i }));
    }
    
    // cargo
    this.rahtiteksti = this.add.text(100, 300, cargotext,
				     { font: '32px Arial', fill: '#ffff00' });
    this.rahti = this.matter.add.gameObject(this.rahtiteksti)
      .setFrictionAir(0.01).setMass(10).setBounce(0.5);
    this.rahti.body.label = CARGO;

    // player ship
    this.alus = this.matter.add.image(500, 150, 'alus');
    this.alus.setBody({ type: 'circle', radius: 33 });
    this.alus.setFrictionAir(0.01);
    this.alus.setMass(10);
    this.alus.setFixedRotation();
    this.alus.label = PLAYER;

    // scoretext
    this.pisteteksti = this.add.text(50,500,'pisteet: 0',
				     { font: '32px Arial', fill: '#ff0011' });

    // joints
    this.rope = null;
    this.loadrope = null;

    // hook
    this.hook = null;

    // hook button
    this.input.keyboard.on('keydown-A', this.fire_hook, this);

    // arrow keys
    cursors = this.input.keyboard.createCursorKeys();

    // init question
    this.get_question();

    // collision handler
    this.matter.world.on("collisionstart", (event) => {
      let pairs = event.pairs;
      for(let i = 0; i < pairs.length; i++){
	let body_a = pairs[i].bodyA;
	let body_b = pairs[i].bodyB;

	if(this.state === game_state["carry"]){
	  if(pairs[i].isSensor){
	    let target_body;
            let player_body;
	    if(body_a.isSensor){
	      target_body = body_a;
              player_body = body_b;
            }else{
	      target_body = body_b;
              player_body = body_a;
	    }
	    this.remove_rope();
	    if(this.loadrope){
	      console.log("error: loadrope already exists!");
	    }else{
	      this.loadrope = this.matter.add.spring(target_body, this.rahti, 120, 0.001);
	    }
	    this.state = game_state["loading"];
	    timer = this.time.addEvent({ delay: 3000,
					 callback: this.unload,
					 callbackScope: this });
	    this.send_answer(target_body.label);
	    console.log("vastasit ",target_body.label);
	  }
	}else if(this.state === game_state["search"]){
	  if(body_a.label === HOOK || body_b.label === HOOK){
	    if(body_a.label === CARGO || body_b.label === CARGO){
	      this.remove_hook();
	      this.rope = this.matter.add.spring(this.alus, this.rahti, 120, 0.001);
	      this.state = game_state["carry"];
	    }
	  }
	}
      }
    });

  }

  get_question(){
    $.ajax({
      type: 'GET',
      url: '/question',
      success: (msg) => {
	this.rahtiteksti.text = msg['q'];
	for(let i = 0; i < 4; i++){
	  this.target_texts[i].text = msg['a'][i];
	}
	this.rahti.x = 600;
	this.rahti.y = 400;
	this.rahti.setAngularVelocity(0);
	this.rahti.setVelocity(0);
	this.rahti.setRotation(0);
	this.rahti.visible = true;
      },
      error: (msg) => {
	console.log(msg);
      }
    });
  }

  send_answer(x){
    $.ajax({
      type: 'GET',
      data: { 'ans': x },
      url: '/answer',
      success: (msg) => {
	pisteet += msg['score'];
	this.pisteteksti.text = "pisteet: " + pisteet;
      },
      error: (msg) => {
	console.log(msg);
      }
    });
  }

  unload(e){
    console.log("unloading");
    this.matter.world.removeConstraint(this.loadrope);
    this.loadrope = null;
    this.rahti.visible = false;
    this.get_question();
    this.state = game_state["search"];
  }

  fire_hook(e){
    if(this.hook){
      console.log("fire_hook(): removing old hook");
      this.remove_hook();
    }
    if(this.state === game_state["search"]){
      console.log("fire_hook(): firing hook");
      if(this.hook){ console.log("error: hook should not exist when firing"); }
      if(this.rope){ console.log("error: rope should not exist when firing"); }
      let angle = Phaser.Math.Angle.Between(this.alus.body.position.x,
					    this.alus.body.position.y,
					    this.rahti.body.position.x,
					    this.rahti.body.position.y);
      this.hook = this.matter.add.rectangle(this.alus.body.position.x +
					    (50) * Math.cos(angle),
					    this.alus.body.position.y +
					    (50) * Math.sin(angle),
					    10, 10);
      this.hook.label = HOOK;
      Phaser.Physics.Matter.Matter.Body.setVelocity(this.hook, {
	x: 10.0 * Math.cos(angle),
	y: 10.0 * Math.sin(angle)
      });
    }else if(this.state === game_state["carry"]){
      console.log("fire_hook(): removing joint");
      if(this.rope){
	this.remove_rope();
	this.state = game_state["search"];
      }else{
	console.log("error: game state is carry without a rope");
      }
    }
  }

  remove_hook(){
    this.matter.world.remove(this.hook);
    this.hook = null;
  }

  remove_rope(){
    if(this.state === game_state["carry"]){
      this.matter.world.removeConstraint(this.rope);
      this.rope = null;
    }else{
      console.log("error: remove_rope called while not in carry");
    }
  }

  update(time, delta){
    if(this.state.value === "search" || this.state.value === "carry" ||
      this.state.value === "loading"){
      if(cursors.up.isDown){ this.alus.thrust(0.01); }
      // if(cursors.down.isDown){ this.get_question(); }
      if(cursors.left.isDown){
	this.alus.setAngularVelocity(-0.1);
      }else if(cursors.right.isDown){
	this.alus.setAngularVelocity(0.1);
      }else{
	this.alus.setAngularVelocity(0.0);
      }
    }
  }
  
}

window.onload = () => {
  const config = {
    type: Phaser.AUTO,
    scale: {
      mode: Phaser.Scale.FIT,
      autoCenter: Phaser.Scale.CENTER_BOTH,
      parent: 'peli',
      width: 1200,
      height: 600
    },
    backgroundColor: '#000010',
    physics: {
      default: 'matter',
      matter: {
	gravity: { y: options.gravity },
	enableSleeping: true,
	debug: {
	  showCollisions: true,
	  collisionColor: 0xf5950c
	}
      }
    },
    scene: Lentorahti
  };

  let game = new Phaser.Game(config);
  window.focus();

};
