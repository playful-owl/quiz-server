'use strict';

$("document").ready( () => {

  let ctx_playersessions = document.getElementById('cplayersessions').getContext('2d');
  let ctx_questions = document.getElementById('cquestions').getContext('2d');

  Chart.defaults.global.defaultFontFamily = "Arial";
  Chart.defaults.global.title.fontSize = 18;
  Chart.defaults.global.legend.fontSize = 15;

  // Question plots
  
  function plot_questions(){
    $.ajax({
      type: 'GET', url: '/api/questions',
      // data: { 'id': selection_id, },
      success: (msg) => {
	if(true){
	  draw_questions(msg['words'],msg['scores'],msg['attempts']);
	}
      },
      error: (msg) => { console.log('dataquestions error ' + msg); }
    });
  }

  function draw_questions(w,s,a){
    const dobj = {
      labels: w,
      datasets: [ { label: "oikein", data: s, backgroundColor: "#69B3A2" },
		  { label: "väärin", data: a, backgroundColor: "#A95362" } ]
    };
    let chartq = new Chart(ctx_questions, {
      type: "bar", data: dobj,
      responsive: true,
      maintainAspectRatio: false,
      options: {
	legend: { display: true },
	title: { display: true, text: ['Kysymykset'] },
	tooltips: { enabled: true },
	scales: {
	  xAxes: [{ stacked: true, ticks: { fontSize: 15 }  }],
	  yAxes: [{ stacked: true  }]
	}
      }
    });
  }

  // Session plots
  
  function plot_sessions(){
    $.ajax({
      type: 'GET', url: '/api/playersessions',
      success: (msg) => {
	console.log(msg);
	if(true){
	  draw_sessions(msg['times'],msg['scores'],msg['attempts']);
	  d3_sessions(msg['times'],msg['scores'],msg['attempts']);
	}
      },
      error: (msg) => { console.log('dataquestions error ' + msg); }
    });
  }

  function draw_sessions(t,s,a){
    const dobj = {
      labels: t,
      datasets: [ { label: "oikein", data: s, backgroundColor: "#69B3A2" },
		  { label: "väärin", data: a, backgroundColor: "#A95362" } ]
    };
    let chartTest = new Chart(ctx_playersessions, {
      type: "bar", data: dobj,
      responsive: false,
      maintainAspectRatio: false,
      options: {
	legend: { display: true },
	title: { display: true, text: ['Sessiot'] },
	tooltips: { enabled: true },
	scales: {
	  xAxes: [{ stacked: true }],
	  yAxes: [{ stacked: true }]
	}
      }
    });
  }

  function d3_sessions(t,s,a){

    console.log("d3sessions");

    let size = t.length;
    
    const margin = {top: 30, right: 30, bottom: 75, left: 80};
    const width = 1100 - margin.left - margin.right;
    const height = 600 - margin.top - margin.bottom;

    const data2 = [
      { session: 1, duration: 3, startat: 0, success: 1 },
      { session: 2, duration: 5, startat: 3, success: 2 },
      { session: 3, duration: 1, startat: 8, success: 4 },
      { session: 4, duration: 3, startat: 9, success: 3 },
    ];

    let data = [];
    let acc = 0;
    for(var i = 0; i < size; i++){
      data.push({
	session: i+1,
	duration: s[i] + a[i],
	startat: acc,
	success: s[i] / (s[i] + a[i])
      });
      acc += s[i] + a[i];
    }

    let svg = d3.select("#d3sessiot").append("svg")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
	.append("g")
	.attr("transform",
	      "translate(" + margin.left + "," + margin.top + ")");

    let x_sum = d3.sum(data, (d) => { return +d.duration;} );
    let x = d3.scaleLinear().range([ 0, width ]).domain([ 0, x_sum ]);
    svg.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(x)).selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    let y_max = d3.max(data, (d) => { return +d.success;} );
    let y = d3.scaleLinear().domain([0, y_max]).range([ height, 0]);
    svg.append("g").call(d3.axisLeft(y));

    let coloursc = d3.scaleSequential().domain([y_max,0])
	.interpolator(d3.interpolateViridis);

    svg.selectAll("mybar").data(data).enter().append("rect")
      .attr("x", (d) => { return x(d.startat); })
      .attr("y", (d) => { return y(d.success); })
      .attr("width", (d) => { return x(d.duration); })
      .attr("height", (d) => { return height - y(d.success); })
      .attr("fill", (d) => { return coloursc(d.success); });

    svg.append("text")
      .attr("text-anchor", "end")
      .attr("x", (width / 2.0) + 30)
      .attr("y", height + margin.top + 20)
      .text("answers given");

    svg.append("text")
      .attr("text-anchor", "end")
      .attr("transform", "rotate(-90)")
      .attr("y", -margin.left + 30)
      .attr("x", -margin.top - 150)
      .text("prop. correct");
  }

  plot_questions();

  plot_sessions();

});
