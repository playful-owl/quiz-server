from datetime import datetime
from koodit import db, login
from werkzeug.security import generate_password_hash, check_password_hash
from marshmallow_sqlalchemy import ModelSchema
from marshmallow import fields
from flask_login import UserMixin

class Quiz(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    word = db.Column(db.String(64),unique=True)
    language = db.Column(db.String(10))
    gender = db.Column(db.String(10))
    kind = db.Column(db.String(10))
    chapter = db.Column(db.Integer)
    finnish = db.Column(db.String(64),unique=True)

class QuizSchema(ModelSchema):
    class Meta(ModelSchema.Meta):
        model = Quiz
        sqla_session = db.session
    id = fields.Number(dump_only=True)
    word = fields.String(required=True)
    finnish = fields.String(required=True)

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True)
    password_hash = db.Column(db.String(128))
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))
