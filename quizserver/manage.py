import os
import pandas
from flask.cli import FlaskGroup
from koodit import serveri, db
from koodit.mallit import Quiz, User

cli = FlaskGroup(serveri)

def fun():
    d = pandas.read_csv('data/sanat4.csv',sep=';')
    for i in range(d.shape[0]):
        cword = d.iloc[i,0]
        objs = Quiz.query.filter_by(word=cword).all()
        if len(objs) == 0:
            clanguage = d.iloc[i,1]
            cgender = d.iloc[i,2]
            ckind = d.iloc[i,3]
            cchapter = int(d.iloc[i,4])
            cfinnish = d.iloc[i,5]
            koko = cword+' '+clanguage+' '+cgender+' '+ckind+' '+str(cchapter)+' '+cfinnish
            q = Quiz(word=cword,language=clanguage,gender=cgender,
                    kind=ckind,chapter=cchapter,finnish=cfinnish)
            db.session.add(q)
            db.session.commit()

@cli.command("create_db")
def create_db():
    db.drop_all()
    db.create_all()
    db.session.commit()
    fun()
    u0 = User(username='jani')
    u0.set_password(os.getenv("USER_PASS"))
    db.session.add(u0)
    db.session.commit()

if __name__ == "__main__":
    cli()
